package omsu.javaprojects.arity;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ArityTest {
    @Test
    public void testLinearArity() {
        Function1Arity lin = new Linear1Arity(1, 1, 0, 3);
        assertEquals(lin.getValueAtPoint(1), 2, 10E-9);
    }

    @Test(expected = RuntimeException.class)
    public void testDivArity() {
        Function1Arity div1 = new Div1Arity(1, 1, 0, 0, 0, 8);
        div1.getValueAtPoint(1);

    }

    @Test
    public void testDivArityV2() {
        Function1Arity div1 = new Div1Arity(2, 3, 2, 3, -1, 1);
        assertEquals(div1.getValueAtPoint(0), 1, 10E-9);
    }


    @Test
    public void testSinArity() {
        Function1Arity sin = new Sin1Arity(1, 1, 0, 1);
        assertEquals(sin.getValueAtPoint(0), 0, 10E-9);
    }

    @Test(expected = RuntimeException.class)
    public void testSinArityDomain() {
        Function1Arity sin = new Sin1Arity(1, 1, 0, 1);
        assertEquals(sin.getValueAtPoint(-10), 0, 10E-9);
    }

    @Test
    public void testExpArity() {
        Function1Arity ex1 = new Exp1Arity(1, 1, 0, 4);
        assertEquals(3.7, ex1.getValueAtPoint(1), 0.1);
        Function1Arity ex2 = new Exp1Arity(1, 1, 0, 4);
        assertEquals(2, ex2.getValueAtPoint(0), 10E-9);
    }
}
