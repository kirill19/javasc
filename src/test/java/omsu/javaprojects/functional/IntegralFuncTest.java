package omsu.javaprojects.functional;

import omsu.javaprojects.arity.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IntegralFuncTest {
    @Test
    public void testFunctionalLin() {
        IntegralFunc fun = new IntegralFunc(0, 1);
        Function1Arity lin = new Linear1Arity(1, 0, 0, 1);
        assertEquals(0.5, fun.functional(lin), 0.1);
    }

    @Test
    public void testFunctionalDiv() {
        IntegralFunc fun = new IntegralFunc(0, 1);
        Function1Arity div = new Div1Arity(1, 0, 0, 1, 0, 1);
        assertEquals(0.5, fun.functional(div), 0.1);
    }

    @Test
    public void testFunctionalSin() {
        IntegralFunc fun = new IntegralFunc(0, Math.PI/2.);
        Function1Arity sin = new Sin1Arity(2, 1, 0, 4);
        System.out.println(fun.functional(sin));
        //assertEquals(2,fun.functional(sin), 0.1);
        assertEquals(2,fun.functional(sin), 1e-3);
    }
}
