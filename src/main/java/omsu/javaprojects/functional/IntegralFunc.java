package omsu.javaprojects.functional;

import omsu.javaprojects.arity.Function1Arity;

public class IntegralFunc <T extends Function1Arity> implements  PolyarityFunctional<T> {
    private double leftBound;
    private double rightBound;

    public IntegralFunc(double leftBound, double rightBound) {
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    public double functional(T func) {
        double h = (rightBound - leftBound) / Config.DIVS;
        double ans = 0;
        for (double i = 0; i < Config.DIVS; i ++) {
            ans += func.getValueAtPoint(leftBound + h*i)*h;
        }
        return ans;
    }
}
