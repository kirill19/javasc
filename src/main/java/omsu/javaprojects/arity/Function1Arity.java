package omsu.javaprojects.arity;

public interface Function1Arity {
    double getValueAtPoint(double x);
    double getLeft();
    double getRight();
}
