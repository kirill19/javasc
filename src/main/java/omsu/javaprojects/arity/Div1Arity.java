package omsu.javaprojects.arity;

public class Div1Arity implements Function1Arity {
    //f(x) = (Ax+b)/(Cx+d);
    private double a;
    private double b;
    private double c;
    private double d;
    private double left;
    private double right;

    public Div1Arity(double a, double b, double c, double d, double left, double right) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        if(left<right) {
            this.left = left;
            this.right = right;
        }else {
            this.left = right;
            this.right = left;
        }
    }

    public double getValueAtPoint(double x) {
        if (c * x + d <= 10E-9 || x > right || x < left) {
            throw new RuntimeException();
        } else {
            return (a * x + b) / (c * x + d);
        }
    }

    @Override
    public double getLeft() {
        return left;
    }

    @Override
    public double getRight() {
        return right;
    }
}
